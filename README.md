# Usegalaxy.fr tools

This repository installs Galaxy tools on [usegalaxy.fr](https://usegalaxy.fr/)

It is based on the Ansible role [galaxyproject/galaxy-tools](https://github.com/galaxyproject/ansible-galaxy-tools), and almost follows the usegalaxy.eu process [usegalaxy-eu/usegalaxy-eu-tools](https://github.com/usegalaxy-eu/usegalaxy-eu-tools).

Data Managers are managed on another (https://gitlab.com/ifb-elixirfr/usegalaxy-fr/data_manager/)[Git repository].

## How to contribute

If you need a tool that is not yet installed on usegalaxy.fr, you can freely contribute to this repository.

The list of tools is contained in multiple `.yml` files in the [`files` directory](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/tools/-/tree/master/files/). The `.yml.lock` files contains the list of specific versions of tools that are installed on usegalaxy.fr, they are managed automatically, you most likely don't need to modify them.

## Install a new tool

1. Spot the appropriate tool list in [files/](files/)
2. Add your tool in the `.yml` file (you can use the built-in editor of GitLab, or edit on a local clone)
3. Create a Merge Request to propose your new tool

Example to add the tool `sicer` from the `devteam` ToolShed user, in the `Peak Calling` section in the Galaxy tool menu:

[files/peak_calling.yml](files/peak_calling.yml)
```yaml
tools:
- name: sicer
  owner: devteam
  tool_panel_section_label: Peak Calling
```

Once your merge request is accepted, the tool will be added to this repository, but will not be installed immediately on usegalaxy.fr. It will only be installed automatically during the next weekly automatic tool update (see below), which will automatically fill the `.yml.lock` file with the latest tool version.

## Updating an existing tool

Each week, an automatic process updates all the tools installed in this repository. A manual update can be forced by running manually the scheduled CI task.

## Tool panel

### Order within the tool panel

After each tool installation, the tool panel is automaticaly sorted to match the order in yaml files. (for now only for workflow4metabolomics, proteore and galaxyP)
You can be add to the "sorted panel list", so if you want a "chronology" in your tool, you need to sort them in the `.yml` file

### Missing categories

If a category is missing, you can add it using this [tool_conf.ym](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_conf.xml).

You will need to add it to [the validation schema](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/tools/-/blob/master/.schema.yaml) too.

### Subdomains

The different subdomains are managed using filters here : [global_host_filters.py.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/global_host_filters.py.j2)

## sanitize_whitelist.txt

If a tool need to display html as output. It have to be in this [sanitize_whitelist.txt](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/sanitize_whitelist.txt)
